---
layout: cv
title: Wadie Moutawakil
---
# /* Wadie Moutawakil */
@brief: _embedded_dev <br/>
@email: <a href="mailto:wadie.moutawakil@gmail.com">wadie.moutawakil@gmail.com</a> <br/>
@web: <a href="https://wadie.cool">wadie.cool</a>

## About
Enthusiastic embedded software engineer.<br/>
2 years' experience in software development on Embedded Linux and Bare Metal platforms.
## Skills
<b>- Main programming languages:</b> C, Modern C++, Python<br/>
<b>- Other programming languages:</b> Rust, Matlab/Octave, Web (JS, HTML, CSS), Java<br/>
<b>- Software:</b> Multithreading, Inter-process communication, Some Kernel, etc.<br/>
<b>- Misc:</b> Git, Bash, Jenkins, Docker, Jira, etc.<br/>
<b>- Creative:</b> Audacity, Inkscape, Gimp, Adobe Creative Suite<br/>
<b>- Languages:</b> French (native), English (fluent), Arabic (native)

## Experience

`08/2020 🠓 Current`
__[Sagemcom](https://www.sagemcom.com) - Embedded software engineer (Paris area, France)__

Developing software for next generation Wi-Fi Gateways and Extenders.<br/>
> Coding: 80% C++, 20% C // OS: OpenWRT // Chips: BCM43684, BCM6750

<u> I. Design and development of a <a href="https://www.wi-fi.org/discover-wi-fi/wi-fi-easymesh">Wi-Fi Certified EasyMesh™</a> Multi-AP Controller</u> <br/>
\- Designed and currently maintaining the abstraction layer between [prplMesh](https://gitlab.com/prpl-foundation/prplmesh) and [Swan](https://www.sagemcom.com/en/broadband-solutions/software-solutions). <br/>
\- Updated Sagemcom's legacy device steering and roaming algorithm to support [IEEE1905.1](https://standards.ieee.org/standard/1905_1-2013.html). <br/>
\- Coded and co-designed a new optimal topology selection algorithm. <br/>
\- Fixed multiple bugs in a fast paced environment and participated in multiple remote sessions with certification labs. <br/>


<u> II. Design and development of a <a href="https://www.wi-fi.org/discover-wi-fi/wi-fi-easymesh">Wi-Fi Certified EasyMesh™</a> Multi-AP Agent</u> <br/>
\- Designed and coded multiple features of the Agent: 802.11v/k frames management, link metrics collection, Wi-Fi scan, topology management, etc. <br/>
\- Re-factored parts of the [prplMesh](https://gitlab.com/prpl-foundation/prplmesh) Agent to ease its integration on Sagemcom's software platform. <br/>
\- Contributed to the refactoring of Sagemcom's HAL to support [EasyMesh™](https://www.wi-fi.org/discover-wi-fi/wi-fi-easymesh) features. <br/>

`02/2020 🠓 08/2020`
__[Withings](https://www.withings.com/fr/fr/) - Embedded software engineer (Paris area, France)__

In charge of the maintenance and improvement of common software components.<br/>
> Coding: 90% C, 10% Python // OS: FreeRTOS // Chips: nRF52xxx, NXP Kinetis K22, BCM43438

<u> I. Encryption of all user data stored in an external flash</u> <br/>
\- Re-designed the internal database API to support encryption using TLS1.3 [(Cifra)](https://github.com/ctz/cifra).<br/>
\- Designed and coded a power fail safe encryption migration routine.<br/>
\- Designed and coded a secure storage scheme for encryption keys.<br/>
\- Re-worked two bootloaders, for two different platforms, to support all the changes stated above.

<u> II. Miscellaneous tasks</u> <br/>
\- Ported part of the BCM43438 Wi-Fi driver from Kinetis K22 to nRF52xxx.<br/>
\- Wrote a driver for the MAX11270 ADC.<br/>
\- Ran power consumption tests on different firmware revisions to measure algorithms power draw.

`02/2019 🠓 11/2019`
__[SoftBank Robotics Europe](https://www.softbankrobotics.com/emea/fr/index) - Audio software engineer (Paris area, France)__

Intern during the first 6 months, then full time employee.<br/>
> Coding: 50% C, 30% Python, 10% C++, 10% Octave // OS: NAOqi (Yocto) // Chips: STM32H7xx/F4xx, Pepper/NAO (Intel Atom x86 based)

<u> I. Firmware development for <a href="https://www.softbankrobotics.com/emea/fr/pepper">Pepper's</a> new audio board</u> <br/>
\- Implemented a USB audio class driver on an [STM32H750](https://www.st.com/en/microcontrollers-microprocessors/stm32h750-value-line.html) based board. <br/>
\- Wrote drivers for the board's DAC and microphones. <br/>
\- Profiled the [Speex DSP](https://www.speex.org/) AEC algorithm and most of [CMSIS's](https://github.com/ARM-software/CMSIS_5) DSP functions.

<u> II. Internship: Speech intelligibility optimization for Pepper</u> <br/>
\- Designed and coded an ambient noise analysis driven dynamic EQ to replicate the [Lombard reflex](https://en.wikipedia.org/wiki/Lombard_effect) in a humanoid robot.<br/>
\- Designed and coded an ambient noise classifier based on [Panotti](https://github.com/drscotthawley/panotti). <br/>
\- Ran subjective tests with the UX team and designed objective tests by automatizing [PESQ](https://en.wikipedia.org/wiki/Perceptual_Evaluation_of_Speech_Quality) computation using audio samples recorded in an anechoic chamber.

<div class="pagebreak"> </div>

## Education

`2015 🠓 2019`
__[ENSEA: École Nationale Supérieure de l'Électronique et de ses Applications](https://www.ensea.fr/fr) (Paris area, France)__

\- MSc in Computer Science, Electrical & Electronics Engineering. <br/>
\- Specialization: Embedded systems and signal processing.

`2013 🠓 2015`
__[CPGE Lycée Pothier](https://www.lycee-pothier.com/) (Orléans, France)__

\- Two years preparation for competitive entrance into top engineering schools. <br/>
\- Specialization: Physics and Engineering. Among the top 30 students.

# []()
References, info about my internships and personal projects available on request. <br/>

<!-- ### Footer

Last updated: May 2021 -->
